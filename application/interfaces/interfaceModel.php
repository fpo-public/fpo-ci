<?php 
interface CrudModel {

public function getAll();

public function getById($id);

public function create($id);

public function update($id);

public function delete($id);

}
interface AuthModel {

public function oauth($id);

public function decode($id);

}


