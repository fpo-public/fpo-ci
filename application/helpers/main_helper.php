<?php 
 if(!function_exists('cleanStringSearch'))
 {
	function cleanStringSearch($string) {
		// $string = str_replace(' ', '', $string); // Replaces all spaces with hyphens.
		$string = str_replace('-', '', $string); // Replaces all spaces with hyphens.
		$string = str_replace('+', '', $string); // Replaces all spaces with hyphens.
		$string = str_replace('*', '', $string); // Replaces all spaces with hyphens.
		$string = str_replace('/', '', $string); // Replaces all spaces with hyphens.
		$string = str_replace('~', '', $string); // Replaces all spaces with hyphens.
		$string = str_replace('!', '', $string); // Replaces all spaces with hyphens.
		$string = str_replace('@', '', $string); // Replaces all spaces with hyphens.
		$string = str_replace('#', '', $string); // Replaces all spaces with hyphens.
		$string = str_replace('$', '', $string); // Replaces all spaces with hyphens.
		$string = str_replace('%', '', $string); // Replaces all spaces with hyphens.
		$string = str_replace('^', '', $string); // Replaces all spaces with hyphens.
		$string = str_replace('&', '', $string); // Replaces all spaces with hyphens.
		$string = str_replace('(', '', $string); // Replaces all spaces with hyphens.
		$string = str_replace(')', '', $string); // Replaces all spaces with hyphens.
		$string = str_replace('{', '', $string); // Replaces all spaces with hyphens.
		$string = str_replace('}', '', $string); // Replaces all spaces with hyphens.
		$string = str_replace('&', '', $string); // Replaces all spaces with hyphens.
		$string = str_replace('|', '', $string); // Replaces all spaces with hyphens.
		$string = str_replace(':', '', $string); // Replaces all spaces with hyphens.
		$string = str_replace('<', '', $string); // Replaces all spaces with hyphens.
		$string = str_replace('>', '', $string); // Replaces all spaces with hyphens.
		$string = str_replace('?', '', $string); // Replaces all spaces with hyphens.
		$string = str_replace('[', '', $string); // Replaces all spaces with hyphens.
		$string = str_replace(']', '', $string); // Replaces all spaces with hyphens.
		$string = str_replace('\\', '', $string); // Replaces all spaces with hyphens.
		
		return $string;
		// return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
	 }
 }
 
if(!function_exists('time_elapsed_string'))
{
	function time_elapsed_string($ptime)
	{
		$sum_time_now = strtotime($ptime);
		$etime = time() - $sum_time_now ;//host delay 7h
	
		if ($etime < 60)
		{
			return 'Just now';
		}
	
		$a = array( 24 * 60 * 60  =>  'day',
					     60 * 60  =>  'hour',
							  60  =>  'min'
					);
		$a_plural = array( 'day'    => 'days',
						   'hour'   => 'hrs',
						   'min' => 'mins',
					);
	
		foreach ($a as $secs => $str)
		{
			$d = $etime / $secs;
			if ($d >= 1)
			{
				$r = round($d);
				if ($str == "day") {
					if ($r == 1) {
						return "Yesterday";
					} 
					if ($r > 2) {
						return date("F j, Y", mktime(0,0,0,date("m", $sum_time_now), date("d", $sum_time_now), date("Y", $sum_time_now)));
					} 
				}
				return $r . ' ' . ($r > 1 ? $a_plural[$str] : $str);
			}
		}
	}
}

if(!function_exists('toTimeString'))
{
	function toTimeString($ptime)
	{
		$sum_time_now = strtotime($ptime);
		$date = date("j/m/Y", mktime(0, 0, 0, date("m", $sum_time_now), date("d", $sum_time_now), date("Y", $sum_time_now)));
		$time = date("h:ia", mktime(date("h", $sum_time_now),date("i", $sum_time_now),date("s", $sum_time_now),0, 0, 0));	
		return $date . ' at ' . $time;
	}
}
if(!function_exists('checkNoti'))
{
	function checkNoti()
	{
		    if(isset($_SESSION["token"])){
				$param = array();
				$_SESSION['listNoti']=ktDecodeJson(restAPI(LINKAPI."/noti/getByUser",arraytostring($param)));
				$nRead = ktDecodeJson(restAPI(LINKAPI."/noti/getUnreadAmount",arraytostring($param)));
				if($nRead != "" || $nRead != null){
					$_SESSION['unReadNoti']=$nRead->nUnread;
				}
				else 
					$_SESSION['unReadNoti']=0;
			}
			
	}
}



?>