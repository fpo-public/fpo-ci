<?php 
if(!function_exists('restAPI'))
{
  function restAPI($service_url,$curl_post_data)
  {
		$curl = curl_init($service_url);
		$headers = array();
		$headers[] = 'Accept:application/json';
		$headers[] = 'Content-Type:application/json';
		$headers[] = 'x-access-token:'.$_SESSION["token"];
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		$kq=$curl_response;
		curl_close($curl);
		$info=json_decode($kq,true);
		// echo $_SESSION["token"];
		// echo $service_url; debug($info);return; //debug
		return checkResponseAPI($service_url,$info);
	}
}

if(!function_exists('restAPINoToken'))
{
  function restAPINoToken($service_url,$curl_post_data)
  {
    
	$curl = curl_init($service_url);
	$headers = array();
	$headers[] = 'Accept:application/json';
	$headers[] = 'Content-Type:application/json';
	
	curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_POST, true);
	curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
	$curl_response = curl_exec($curl);
	$kq=$curl_response;
	curl_close($curl);

	$info=json_decode($kq,true);
		// echo $service_url; debug($kq);
		// return; //debug

	return checkResponseAPI($service_url,$info);

  }
}
if(!function_exists('restLogin'))
{
  function restLogin($service_url,$curl_post_data)
  {
		$curl = curl_init($service_url);
		$headers = array();
		$headers[] = 'Accept:application/json';
		$headers[] = 'Content-Type:application/json';
		$headers[] = 'x-access-token:'.$_SESSION["token"];
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		$kq=$curl_response;
		curl_close($curl);
		$info=json_decode($kq,true);
		// echo $_SESSION["token"];
		// echo $service_url; debug($info);return; //debug
		return $info;
	}
}


if(!function_exists('checkResponseAPI'))
{
  function checkResponseAPI($service_url,$result)
  {
		$CI = get_instance();
		$CI->load->helper('url');
		try{
			switch($result['exitcode']){
				case 1:
					return $result['data'];
					break;
				case 901:
					$noti = array(
						'icon' => 'glyphicon glyphicon-remove',
						'title' => "",
						'message' => "Vui lòng đăng nhập lại!",
						'url' => "javascript:void(0)",
						'type' => 'warning'
					);
					$_SESSION["THONGBAO"]=$noti;
					session_destroy();
					redirect('/');
					break;
				case 903:
					$noti = array(
						'icon' => 'glyphicon glyphicon-remove',
						'title' => "",
						'message' => "Hết phiên đăng nhập, vui lòng đăng nhập lại!",
						'url' => "javascript:void(0)",
						'type' => 'warning'
					);
					$_SESSION["THONGBAO"]=$noti;
					session_destroy();
					redirect('/');
					break;
				case 905:
					$noti = array(
						'icon' => 'glyphicon glyphicon-remove',
						'title' => "",
						'message' => "Gửi yêu cầu quá nhiều, vui lòng thử lại sau!",
						'url' => "javascript:void(0)",
						'type' => 'warning'
					);
					$_SESSION["THONGBAO"]=$noti;
					return array("exitcode" => "905","mess"=>"Gửi yêu cầu quá nhiều, vui lòng thử lại sau" );
					break;
				default:
					$noti = array(
						'icon' => 'glyphicon glyphicon-remove',
						'title' => "",
						'message' => $result['message'],
						'url' => "javascript:void(0)",
						'type' => 'danger'
					);
					break;
			}
		}
		catch(exception $e){
			$noti = array(
				'icon' => 'glyphicon glyphicon-remove',
				'title' => $service_url,
				'message' => "Kết nối đến server gián đoạn!",
				'url' => "javascript:void(0)",
				'type' => 'danger'
			);
			$_SESSION["THONGBAO"]=$noti;
		}
		
		
  }
}


if(!function_exists('arraytostring'))
{
  function arraytostring($data)
  {
   
		return json_encode($data,JSON_UNESCAPED_UNICODE);
  }
}

if(!function_exists('debug'))
{
  function debug($arr)
  {
	echo "<pre>";
	print_r($arr);
	echo "</pre>";
  }
}




if(!function_exists('time_elapsed_string'))
{
	function time_elapsed_string($ptime)
	{
		$sum_time_now = strtotime($ptime);
		$etime = time() - $sum_time_now ;//host delay 7h
	
		if ($etime < 60)
		{
			return 'Just now';
		}
	
		$a = array( 24 * 60 * 60  =>  'day',
					     60 * 60  =>  'hour',
							  60  =>  'min'
					);
		$a_plural = array( 'day'    => 'days',
						   'hour'   => 'hrs',
						   'min' => 'mins',
					);
	
		foreach ($a as $secs => $str)
		{
			$d = $etime / $secs;
			if ($d >= 1)
			{
				$r = round($d);
				if ($str == "day") {
					if ($r == 1) {
						return "Yesterday";
					} 
					if ($r > 2) {
						return date("F j, Y", mktime(0,0,0,date("m", $sum_time_now), date("d", $sum_time_now), date("Y", $sum_time_now)));
					} 
				}
				return $r . ' ' . ($r > 1 ? $a_plural[$str] : $str);
			}
		}
	}
}

if(!function_exists('toFPODateTime'))
{
	function toFPODateTime($datetime)
	{
		$arrDatetime = explode(" ",$datetime);
		$arrDate=explode('/', $arrDatetime[0]);
		$date = new DateTime();
		$date->setDate((int)$arrDate[2], (int)$arrDate[1], (int)$arrDate[0]);
		$arrTime=explode(':', $arrDatetime[1]);
		$date->setTime((int)$arrTime[0], (int)$arrTime[1], 00);
		$date= $date->format('Y-m-d H:i:s');
		return $date;
	}
}
if(!function_exists('toTimeString'))
{
	function toTimeString($ptime)
	{
		$sum_time_now = strtotime($ptime);
		$date = date("j/m/Y", mktime(0, 0, 0, date("m", $sum_time_now), date("d", $sum_time_now), date("Y", $sum_time_now)));
		$time = date("h:ia", mktime(date("h", $sum_time_now),date("i", $sum_time_now),date("s", $sum_time_now),0, 0, 0));	
		return $date . ' at ' . $time;
	}
}




if(!function_exists('arraytostringJS'))
{
	function arraytostringJS($data,$t="",$s="")
	{
		    $chuoi="";
			foreach($data as $tmp){
				$chuoi.="'".$t.$tmp.$s."',";
			}
			return substr($chuoi,0,-1);;
	}
}

if(!function_exists('isAdmin'))
{
	function isAdmin($class,$func)
	{
		    if(isset($_SESSION['isAdmin']) && $_SESSION['isAdmin'] == true && isset($_SESSION["token"])){
				$param = array();
				$_SESSION['listNoti']=ktDecodeJson(callAPI($this->config->item('linkAPI')."/noti/getByUser",arraytostring($param)));
				$nRead = ktDecodeJson(callAPI($this->config->item('linkAPI')."/noti/getUnreadAmount",arraytostring($param)));
				if($nRead != "" || $nRead != null){
					$_SESSION['unReadNoti']=$nRead->nUnread;
				}
				else 
					$_SESSION['unReadNoti']=0;

				$flag = 0;
				if(isset($_SESSION['listPermission'][$class])){

					if(in_array($func,$_SESSION['listPermission'][$class])) $flag =1;
				}
				if($flag != 1){
					$noti = array(
						'icon' => 'glyphicon glyphicon-remove',
						'title' => "",
						'message' => "Bạn không có quyền truy cập vào chức năng này!",
						'url' => "javascript:void(0)",
						'type' => 'warning'
					);
					$_SESSION["THONGBAO"]=$noti;
					redirect('/admin/page/noPermission');


				}
				$curl_post_data = array(
					"token" =>$_SESSION['token']
				);
				$listPermission = json_decode(callAPItest($this->config->item('linkAPI')."/user/getPermission",arraytostring($curl_post_data)),true);
				$_SESSION['listPermission'] = $listPermission['data'];
				return;
			}
			else{
				$noti = array(
					'icon' => 'glyphicon glyphicon-remove',
					'title' => "",
					'message' => "Bạn không có quyền truy cập vào hệ thông!",
					'url' => "javascript:void(0)",
					'type' => 'warning'
				);
				$_SESSION["THONGBAO"]=$noti;

				redirect('/');
			}
	}
}

if(!function_exists('checkIconrNoti'))
{
	function checkIconrNoti($func,$isRead)
	{
		switch($func){
			case "text":{
				if(!$isRead){
					return "<div class='icon-circle bg-light-green'><i class='material-icons'>comment</i></div>";

				}
				else return "<div class='icon-circle bg-grey'><i class='material-icons'>comment</i></div>";
				break;
			}
			case "exam":{
				if(!$isRead){
					return "<div class='icon-circle bg-cyan'><i class='material-icons'>book</i></div>";

				}
				else return "<div class='icon-circle bg-grey'><i class='material-icons'>book</i></div>";
				break;
			}
			case "news":{
				if(!$isRead){
					return "<div class='icon-circle bg-orange'><i class='material-icons'>mode_edit</i></div>";

				}
				else return "<div class='icon-circle bg-grey'><i class='material-icons'>mode_edit</i></div>";
				break;
			}
			
		default:
			return "<div class='icon-circle bg-purple'><i class='material-icons'>settings</i></div>";
		}
		
	}
}


?>