
<div class="header">
    <h2>SỬA NHÓM</h2>
</div>

<div class="body">
    <form id="group" name="group" method="post" action="<?= base_url(); ?>Group/edit/<?= $group['_id'] ?>" enctype="multipart/form-data">
        <div class="body">
            <div class="row clearfix" id="form-body">
               
                <div class="col-sm-12">
                    <label for="price">Tên nhóm <strong> (*) </strong></label>
                    <div class="form-group">
                        <div class="form-line">
                            <input type="text" class="form-control" value="<?php if(isset($group['name'])) echo $group['name'] ?>" id="name" name="name" placeholder="Admin" required="" />
                        </div>
                    </div>
                </div>
                
                <div class="col-sm-12">
                    <label for="Description">Mô tả </label>
                    <div class="form-group">
                        <div class="form-line">
                            <input type="text" class="form-control" value="<?php if(isset($group['description'])) echo $group['description'] ?>" id="description" name="description" placeholder="Nhóm admin" />
                        </div>
                    </div>
                </div>     
                <div class="col-sm-12">
                    <label for="level">Cấp bậc</label>
                    <div class="form-group">
                        <div class="form-line">
                            <input type="number" class="form-control" value="<?php if(isset($group['level'])) echo $group['level'] ?>" id="level" name="level" value="7"  />
                        </div>
                    </div>
                </div>      
                <button type="submit" class="btn btn-block btn-lg bg-red waves-effect">LƯU</button>
            </div>
        </div>
    </form>
</div>
