<div class="header">
		<a class="btn bg-deep-orange btn-raised pull-right m-t--10 waves-effect waves-light" title="Thêm nhóm" data-toggle="tooltip-t" href="<?= base_url(); ?>group/add">
			<i class="material-icons">add</i>
		</a>
		
	<h2>DANH SÁCH NHÓM</h2>
</div>

<div class="body">
	<div class="row clear-fix">
		<div class="col-sm-12">
			<div class="table-responsive">
				<table id="mytable" class="table table-bordered table-striped dataTable dt-responsive display nowrap" style="margin-top:10px" cellspacing="0">
					<thead>
						<tr>
							<th>#</th>
							<th>Tên nhóm</th>
							<th>Mô tả</th>
							<th>Cấp bậc</th>
							<th class="no-sort">Tác vụ</th>
						</tr>
					</thead>
					<tbody>
							<?php $count=0; foreach($group as $u){ $count++; ?>
							<tr>
								<td><?php echo $count; ?></td>
								<td><?php echo $u['name']; ?></td>
								<td><?php echo $u['description']; ?></td>
								<td><?php echo $u['level']; ?></td>
								
								<td>
									<a href="<?= base_url() ?>group/edit/<?= $u['_id'] ?>" class="btn btn-xs btn-info btn-raised" title="Chỉnh sửa" data-toggle="tooltip-l"><i class="material-icons">mode_edit</i></a>
									<a onclick="checkalert('<?= $u['_id'] ?>')" class="btn btn-xs btn-danger btn-raised" title="Xoá" data-toggle="tooltip-r"><i class="material-icons">delete</i></a>
								</td>
							</tr>
							<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<script>
    $.fn.dataTable.ext.errMode = 'none'; //hide warning datatable
	
	function checkalert(id) {
		swal({
				title: "Bạn đã đã chắn chắn?",
				text: "Hành động này sẽ xóa tất cả các dữ liệu liên quan, và không thể phục hồi",
				type: "warning",
				showCancelButton: true,
				cancelButtonText: "Bỏ qua",
				confirmButtonClass: "btn-danger",
				confirmButtonText: "Đồng ý!",
				closeOnConfirm: false
			},
			function() {
				swal("Thành công!", "Dữ liệu của bạn đã được xóa!", "success");
				window.location.href = "<?= base_url() ?>group/delete/" + id;
			});
	}
	
</script>






