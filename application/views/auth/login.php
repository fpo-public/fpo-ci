<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Attendance - FPO</title>
    <!-- Favicon-->
    <link rel="icon" href="http://fpo.vn/FPO.ico" type="image/x-icon">
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    <!-- Bootstrap Core Css -->
    <link href="<?= base_url() ?>public/assets/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Waves Effect Css -->
    <link href="<?= base_url() ?>public/assets/plugins/node-waves/waves.css" rel="stylesheet" />
    <!-- Animation Css -->
    <link href="<?= base_url() ?>public/assets/plugins/animate-css/animate.css" rel="stylesheet" />
    <!-- Custom Css -->
    <link href="<?= base_url() ?>public/assets/css/style.css" rel="stylesheet">
    <!-- <link href="<?=base_url()?>public/assets/plugins/sweetalert2/sweetalert2.all.min.js" rel="stylesheet" /> -->
    <script src="<?=base_url()?>public/assets/plugins/jquery/jquery.min.js"></script>


</head>
<?php
$this->load->view('layouts/message');
?>
<body class="login-page">
    <div class="login-box">
        
        <div class="card">
        
            <div class="body">
            <div class="logo text-center">
                <img style="width:50%;" src="http://fpo.vn/FPO.ico" alt="">
            </div>
                <form id="sign_in" method="POST">
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control" id="username" placeholder="Tên đăng nhập" required autofocus>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input type="password" class="form-control" id="password" placeholder="Mật khẩu" required>
                        </div>
                    </div>
                    <div class="row">
                        
                        <div class="col-xs-12">
                            <button id="myBtn" class="btn btn-block bg-amber waves-effect" type="submit" >Đăng nhập</button>
                        </div>
                    </div>
                    <div class="row m-t-15 m-b--20">
                        
                        <div class="col-xs-12 align-right">
                            <a href="<?=base_url()?>auth/forgetPassword">Quên mật khẩu?</a>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
    <script src="<?=base_url()?>public/assets/plugins/jquery/jquery.min.js"></script>
    <script src="<?=base_url()?>public/assets/plugins/bootstrap-notify/bootstrap-notify.min.js"></script>
    <!-- <script src="<?=base_url()?>public/assets/plugins/sweetalert2/sweetalert2.all.js"></script> -->
    

    <script>
        
        $('#sign_in').on('submit', function(e){  
            e.preventDefault(); 
            var username =$("#username").val();
            var password = $("#password").val();
            if(!username || !password){
              return;
            }
            else{
              $.post(
                '<?= base_url() ?>auth/login',
                {
                  username: username,
                  password : password
                },function(data){
                    data.trim();
                  if(	data!= "" || data!=null ){
                    var obj = JSON.parse(data);
                    console.log(obj.mess);
                    if(obj.exitcode == "1"){
                      window.location.href='<?php echo base_url();?>';
                    }
                    else{
                        shownoti("glyphicon glyphicon-remove","",obj.mess,"#","warning");

                        // Swal.fire({
                        // position: 'top-end',
                        // type: 'error',
                        // title: obj.mess,
                        // showConfirmButton: false,
                        // timer: 1500
                        // });
                    }
                  }
                  else{
                    shownoti("glyphicon glyphicon-remove","",obj.mess,"#","warning");

                    // Swal.fire({
                    //   position: 'top-end',
                    //   type: 'error',
                    //   title: obj.mess,
                    //   showConfirmButton: false,
                    //   timer: 1500
                    //   });
              }
          })
        }
      })
    
      
	</script>
</body>

</html>