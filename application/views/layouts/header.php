<meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>FPO - CI</title>
    <link rel="icon" href="http://fpo.vn/FPO.ico" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    <script src="<?=base_url()?>public/assets/plugins/jquery/jquery.min.js"></script>

    <link href="<?=base_url()?>public/assets/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="<?=base_url()?>public/assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
    <link href="<?=base_url()?>public/assets/plugins/node-waves/waves.css" rel="stylesheet" />
    <link href="<?=base_url()?>public/assets/plugins/animate-css/animate.css" rel="stylesheet" />
    <link href="<?=base_url()?>public/assets/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
    <link href="<?=base_url()?>public/assets/css/style.css" rel="stylesheet">
    <link href="<?=base_url()?>public/assets/css/themes/all-themes.css" rel="stylesheet" />
    <link href="<?=base_url()?>public/assets/js/sweetalert.css" rel="stylesheet" />

    <style>
        .dataTables_wrapper .dt-buttons a.dt-button {
            background-color: #00b0e4 !important;
        }
    </style>
