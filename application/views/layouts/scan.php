<!DOCTYPE html>
<html>
    
    <head>
        <meta charset="utf-8">
        <title>Attendance | FPO</title>
        <base href="/">
        <link rel="icon" href="http://fpo.vn/FPO.ico" type="image/x-icon">

        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

        <!-- Bootstrap Core Css -->
        <link href="<?= base_url() ?>public/assets/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">
        <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
        <link href="<?= base_url() ?>public/assets/css/themes/all-themes.css" rel="stylesheet" />
        <!-- Jquery Core Js -->
        <script src="<?= base_url() ?>public/assets/plugins/jquery/jquery.min.js"></script>
        <!-- Bootstrap Core Js -->
        <script src="<?= base_url() ?>public/assets/plugins/bootstrap/js/bootstrap.js"></script>	 
        <link href="<?= base_url() ?>public/assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
        <!-- Sweetalert Css -->
        <link href="<?= base_url() ?>public/assets/plugins/sweetalert/sweetalert.css" rel="stylesheet" />
        <!-- Animation Css -->
        <link href="<?= base_url() ?>public/assets/plugins/animate-css/animate.css" rel="stylesheet" />
        <!-- Waves Effect Css -->
        <link href="<?= base_url() ?>public/assets/plugins/node-waves/waves.css" rel="stylesheet" />
        <!-- Custom Css -->
        <link href="<?= base_url() ?>public/assets/css/style.css" rel="stylesheet">
        <!-- notification -->
        <link href="<?=base_url()?>public/assets/css/notification.css" rel="stylesheet">

    </head>
    <?php include("message.php"); ?>
    <?php include("socket.php"); ?>

    
	<body class="theme-amber">
        <?php if(isset($_view) && $_view)
            $this->load->view($_view);
        ?>
    </body>

    <script src="<?= base_url() ?>public/assets/plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script>
    <!-- Select Plugin Js -->
    <script src="<?= base_url() ?>public/assets/plugins/bootstrap-select/js/bootstrap-select.js"></script>
    <!-- Slimscroll Plugin Js -->
    <script src="<?= base_url() ?>public/assets/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
	 <!-- Bootstrap Notify Plugin Js -->
    <script src="<?= base_url() ?>public/assets/plugins/bootstrap-notify/bootstrap-notify.js"></script>
    <!-- Waves Effect Plugin Js -->
    <script src="<?= base_url() ?>public/assets/plugins/node-waves/waves.js"></script>
	 <script src="<?= base_url() ?>public/assets/js/pages/ui/notifications.js"></script>
	 <!-- Main Js -->
    <script src="<?= base_url() ?>public/assets/js/main.js"></script>
	<!-- Custom Js -->
    <script src="<?= base_url() ?>public/assets/js/admin.js"></script>
	 	<!-- Dropzone Plugin Js -->
    <script src="<?= base_url() ?>public/assets/plugins/dropzone/dropzone.js"></script>
    <!-- SweetAlert Plugin Js -->
    <script src="<?= base_url() ?>public/assets/plugins/sweetalert/sweetalert.min.js"></script>

    

</html>



