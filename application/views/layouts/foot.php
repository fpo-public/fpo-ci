
    <script>
        $('.dataTable').DataTable({
    responsive: true,
    stateSave: true,
    processing: true,
    "language":
    {
        "decimal": "",
        "emptyTable": "Dữ liệu rỗng",
        "info": "Hiển thị từ _START_ đến _END_ trong tổng cộng _TOTAL_ dòng",
        "infoEmpty": "Dữ liệu rỗng",
        "infoFiltered": "(tìm kiếm từ _MAX_ dòng)",
        "infoPostFix": "",
        "thousands": ",",
        "lengthMenu": 'Hiển thị _MENU_ dòng',
        "loadingRecords": "Đang tải...",
        "processing": "Đang xử lý...",
        "search": '<label class="control-label">Tìm kiếm</label>',
        "zeroRecords": "Không tìm thấy",
        "paginate": {
            "first": "Đầu",
            "last": "Cuối",
            "next": "»",
            "previous": "«"
        },
        "aria": {
            "sortAscending": ": activate to sort column ascending",
            "sortDescending": ": activate to sort column descending"
        }
    },
    "aoColumnDefs": [{
        "bSortable": false,
        "aTargets": ["no-sort"]
    }]
});
    </script>
