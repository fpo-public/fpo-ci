<!DOCTYPE html>
<html>
    <?php include("header.php"); ?>
    <?php include("footer.php"); ?> 
    <?php include("navbar.php"); ?>
    <?php include("sideBar.php"); ?>
    <?php include("message.php"); ?>
    <!-- <?php include("socket.php"); ?> -->
	<body class="theme-amber">
        <section class="content">
            <div class="container-fluid">
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="card">
                            <?php	if(isset($_view) && $_view)
                                $this->load->view($_view);
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </body>
    <?php include("foot.php"); ?> 

</html>



