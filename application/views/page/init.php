<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Init - FPO</title>
    <!-- Favicon-->
    <link rel="icon" href="http://fpo.vn/FPO.ico" type="image/x-icon">
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    <!-- Bootstrap Core Css -->
    <link href="http://file.fpo.vn/fpo-ci/public/assets/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Waves Effect Css -->
    <link href="http://file.fpo.vn/fpo-ci/public/assets/plugins/node-waves/waves.css" rel="stylesheet" />
    <!-- Animation Css -->
    <link href="http://file.fpo.vn/fpo-ci/public/assets/plugins/animate-css/animate.css" rel="stylesheet" />
    <!-- Custom Css -->
    <link href="http://file.fpo.vn/fpo-ci/public/assets/css/style.css" rel="stylesheet">
    <link href="http://file.fpo.vn/fpo-ci/public/assets/plugins/sweetalert/sweetalert.css" rel="stylesheet" />
    <script src="http://file.fpo.vn/fpo-ci/public/assets/plugins/jquery/jquery.min.js"></script>


</head>
<?php
$this->load->view('layouts/message');
?>

<body class="login-page">
    <div class="login-box">

        <div class="card">

            <div class="body">
                <div class="logo text-center">
                    <img style="width:50%;" src="http://fpo.vn/FPO.ico" alt="">
                </div>
                <form id="init" method="POST">
                    <div class="form-group">
                        <label class="form-check-label">
                            Base url
                        </label>

                        <div class="form-line">
                            <input type="text" class="form-control" id="baseUrl" placeholder="http://localhost/fpo-ci/" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="form-check-label">
                            Link API
                        </label>
                        <div class="form-line">
                            <input type="text" class="form-control" id="ipLink" placeholder="68.183.232.204:3001" required>
                        </div>
                    </div>
                    <!-- <div class="form-group">
                        <div class="form-check">
                        <input class="form-check-input" type="checkbox" id="permission" name="permission">
                        <label class="form-check-label" for="gridCheck">
                            Tick if server required 777 permission to write file
                        </label>
                        </div>
                    </div> -->
                    <div class="row">

                        <div class="col-xs-12">
                            <button id="myBtn" class="btn btn-block bg-amber waves-effect" type="submit">Lưu</button>
                        </div>
                    </div>
                    <div class="row m-t-15 m-b--20">

                        <div class="col-xs-12 align-right">
                            <!-- <a href="<?= base_url() ?>auth/forgetPassword">Quên mật khẩu?</a> -->
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
    <script src="http://file.fpo.vn/fpo-ci/public/assets/plugins/jquery/jquery.min.js"></script>
    <script src="http://file.fpo.vn/fpo-ci/public/assets/plugins/bootstrap-notify/bootstrap-notify.min.js"></script>
    <!-- <script src="http://file.fpo.vn/fpo-ci/public/assets/plugins/sweetalert/sweetalert.min.js"></script> -->
    <script src="<?= base_url() ?>public/assets/plugins/sweetalert2/sweetalert2.all.js"></script>



    <script>
        $('#init').on('submit', function(e) {
            e.preventDefault();
            var baseUrl = $("#baseUrl").val();
            var ipLink = $("#ipLink").val();
            var permission = $("#permission").val();
            if (!baseUrl || !ipLink) {
                return;
            } else {
                $.post(
                    '<?= base_url() ?>Install/config', {
                        baseUrl: baseUrl,
                        ipLink: ipLink,
                        permission: permission
                    },
                    function(data) {
                        // console.log(data);return;
                        data.trim();
                        if (data != "" || data != null) {
                            var obj = JSON.parse(data);
                            console.log(obj.mess);
                            if (obj.exitcode == "1") {
                                let timerInterval
                                Swal.fire({
                                    title: 'Installing',
                                    html: 'Installation will complete in <strong></strong> seconds.',
                                    timer: 2000,
                                    onBeforeOpen: () => {
                                        Swal.showLoading()
                                        timerInterval = setInterval(() => {
                                            Swal.getContent().querySelector('strong')
                                                .textContent = Swal.getTimerLeft()
                                        }, 100)
                                    },
                                    onClose: () => {
                                        clearInterval(timerInterval)
                                    }
                                }).then((result) => {
                                    if (
                                        // Read more about handling dismissals
                                        result.dismiss === Swal.DismissReason.timer
                                    ) {
                                        // console.log('I was closed by the timer')
                                        window.location.href = '<?php echo base_url("Auth"); ?>';

                                    }
                                })



                            } else {
                                shownoti("glyphicon glyphicon-remove", "", obj.mess, "#", "warning");
                            }
                        } else {
                            shownoti("glyphicon glyphicon-remove", "", obj.mess, "#", "warning");
                        }
                    })
            }
        })
    </script>
</body>

</html>