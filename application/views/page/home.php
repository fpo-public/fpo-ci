<div class="header">
    <h2>TỔNG QUAN</h2>
</div>
<script src="<?=base_url()?>public/assets/plugins/jquery-countto/jquery.countTo.js"></script>

<div class="body">
    <div class="row clearfix">
        <div class="col-lg-6 col-lg-6 col-sm-6 col-xs-12">
            <div class="info-box bg-pink hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">person_pin_circle</i>
                </div>
                <div class="content">
                    <div class="text">Luợt checkin</div>
                    <div class="number count-to" data-from="0" data-to="<?php if(isset($report['nAttendance'])) echo $report['nAttendance']; ?>" data-speed="1000" data-fresh-interval="20"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-lg-6 col-sm-6 col-xs-12">
            <div class="info-box bg-light-green hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">meeting_room</i>
                </div>
                <div class="content">
                    <div class="text">Tổng sự kiện</div>
                    <div class="number count-to" data-from="0" data-to="<?php if(isset($report['nEvent'])) echo $report['nEvent']; ?>" data-speed="1000" data-fresh-interval="20"></div>                            
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-lg-6 col-sm-6 col-xs-12">
            <div class="info-box bg-cyan hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">contacts</i>
                </div>
                <div class="content">
                    <div class="text">Tổng số khách mời</div>
                    <div class="number count-to" data-from="0" data-to="<?php if(isset($report['nParticipan'])) echo $report['nParticipan']; ?>" data-speed="1000" data-fresh-interval="20"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-lg-6 col-sm-6 col-xs-12">
            <div class="info-box bg-orange hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">person_outline</i>
                </div>
                <div class="content">
                    <div class="text">Tổng số user</div>
                    <div class="number count-to" data-from="0" data-to="<?php if(isset($report['nUser'])) echo $report['nUser']; ?>" data-speed="1000" data-fresh-interval="20"></div>
                </div>
            </div>
        </div>
    </div>

    </div>
</div>
<script>
$(function() {
            //Widgets count
            $('.count-to').countTo();

            //Sales count to
            $('.sales-count-to').countTo({
                formatter: function (value, options) {
                    return '$' + value.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, ' ').replace('.', ',');
                }
            });
        });

</script>


