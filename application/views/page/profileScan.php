<section class="content-profile">
  <div class="container-fluid">
    <div class="block-header">
      <h2></h2>
    </div>
    <div class="row clearfix m-t-40">
      <div class="col-lg-1 hide-md hide-sm hide-xs"></div>
      <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
        
          <div class="header">
            <img class="profile-avatar" src="http://pickup.fpo.vn/avatar\1.png" id="banner" width="160" height="160" alt="User">
            <!-- <a class="profile-avatar-edit" data-type="html-message"><i
                class="material-icons">photo_camera</i>Đổi ảnh</a> -->
                <a class="btn bg-blue-grey btn-xs pull-right m-t--10 waves-effect waves-light" title="Quay lại trang chủ" data-toggle="tooltip-l" href="javascript:window.history.back();">
          <i class="material-icons">home</i>
        </a>
            <h2 id="fullnameh2" style="font-size:30px;margin-left:200px">fullname</h2>
          </div>
          <div class="body" style="padding-top: 70px!important;">
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group label-floating">
                    <b>Tên đăng nhập *</b>
                    <div class="input-group">
                      <span class="input-group-addon">
                        <i class="material-icons">account_circle</i>
                      </span>
                      <div class="form-line">
                        <input type="text" id="username" value="username" name="username"
                          class="form-control" placeholder="Vd: tendangnhap123" required readonly>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group label-floating">
                    <b>Họ & tên *</b>
                    <div class="input-group">
                      <span class="input-group-addon">
                        <i class="material-icons">perm_identity</i>
                      </span>
                      <div class="form-line">
                        <input type="text" id="fullname" name="fullname" value="fullname"
                          class="form-control" placeholder="Vd: Họ Văn Tên" required>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group label-floating">
                    <b>Điện thoại</b>
                    <div class="input-group">
                      <span class="input-group-addon">
                        <i class="material-icons">phone</i>
                      </span>
                      <div class="form-line">
                        <input type="text" class="form-control phone" id="phone" name="phone" value=""
                          placeholder="Vd: 0900 9999 999">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group label-floating">
                    <b>E-mail *</b>
                    <div class="input-group">
                      <span class="input-group-addon">
                        <i class="material-icons">email</i>
                      </span>
                      <div class="form-line">
                        <input type="text" class="form-control email" id="email" name="email"
                          value="contact@fpo.vn" placeholder="Vd: user@domain.vn" required>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-sm-10">
                  <div class="form-group label-floating">
                    <b>Facebook</b>
                    <div class="input-group">
                      <span class="input-group-addon">
                        <i class="material-icons">assignment_ind</i>
                      </span>
                      <div class="form-line">
                        <input type="text" class="form-control" id="facebook" name="facebook" value="https://facebook.com/fpofpofpo"
                          placeholder="Vd: https://facebook.com/______">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-sm-2">
                        
                </div>
              </div>

              <div class="row">
                <div class="col-sm-12">
                  <div class="form-group label-floating">
                    <b>Nơi làm việc / Đơn vị</b>
                    <div class="input-group">
                      <span class="input-group-addon">
                        <i class="material-icons">domain</i>
                      </span>
                      <div class="form-line">
                        <input type="text" class="form-control" id="work" name="work" value="FPO"
                          placeholder="Vd: Công nghệ Thông tin">
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-sm-12">
                  <div class="form-group label-floating">
                    <b>Địa chỉ</b>
                    <div class="input-group">
                      <span class="input-group-addon">
                        <i class="material-icons">location_on</i>
                      </span>
                      <div class="form-line">
                        <input type="text" class="form-control" id="address" name="address" value="Tây Ninh"
                          placeholder="Vd: 280 An Dương Phương, phường 4, quận 5">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
          </div>
        </div>
      </div>
      <div class="col-lg-1 hide-md hide-sm hide-xs"></div>
    </div>
  </div>
</section>
<script>
var dataInfo = JSON.parse(sessionStorage.getItem("ParticipantData"));
// console.log(dataInfo);
if(dataInfo != null){
  $('#username').val(dataInfo.username);
  $('#fullname').val(dataInfo.fullname);
  $('#phone').val(dataInfo.phone);
  $('#email').val(dataInfo.email);
  $('#facebook').val(dataInfo.facebook);  
  $('#address').val(dataInfo.address);
  $('#work').val(dataInfo.work);
  $('#banner').attr('src','<?= base_url();?>'+dataInfo.avatar);
  $('#fullnameh2').text(dataInfo.fullname);
  shownoti("glyphicon glyphicon-star","Chào mừng đại biểu ",dataInfo.fullname,"","success");
}

</script>

