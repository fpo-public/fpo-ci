<style>
    .el-header {
        background-color: #ffbf00;
        color: #333;
        line-height: 60px;
    }

    .el-aside {
        color: #333;
    }
</style>
<div id="app">
    <el-container>
        <el-aside width="200px" style="background-color: rgb(238, 241, 246)">
            <el-menu :default-openeds="['1', '3']">
                <el-submenu index="1">
                    <template slot="title"><i class="el-icon-message"></i>Navigator One</template>
                    <el-menu-item-group>
                        <template slot="title">Group 1</template>
                        <el-menu-item index="1-1">Option 1</el-menu-item>
                        <el-menu-item index="1-2">Option 2</el-menu-item>
                    </el-menu-item-group>
                    <el-menu-item-group title="Group 2">
                        <el-menu-item index="1-3">Option 3</el-menu-item>
                    </el-menu-item-group>
                    <el-submenu index="1-4">
                        <template slot="title">Option4</template>
                        <el-menu-item index="1-4-1">Option 4-1</el-menu-item>
                    </el-submenu>
                </el-submenu>
                <el-submenu index="2">
                    <template slot="title"><i class="el-icon-menu"></i>Navigator Two</template>
                    <el-menu-item-group>
                        <template slot="title">Group 1</template>
                        <el-menu-item index="2-1">Option 1</el-menu-item>
                        <el-menu-item index="2-2">Option 2</el-menu-item>
                    </el-menu-item-group>
                    <el-menu-item-group title="Group 2">
                        <el-menu-item index="2-3">Option 3</el-menu-item>
                    </el-menu-item-group>
                    <el-submenu index="2-4">
                        <template slot="title">Option 4</template>
                        <el-menu-item index="2-4-1">Option 4-1</el-menu-item>
                    </el-submenu>
                </el-submenu>
                <el-submenu index="3">
                    <template slot="title"><i class="el-icon-setting"></i>Navigator Three</template>
                    <el-menu-item-group>
                        <template slot="title">Group 1</template>
                        <el-menu-item index="3-1">Option 1</el-menu-item>
                        <el-menu-item index="3-2">Option 2</el-menu-item>
                    </el-menu-item-group>
                    <el-menu-item-group title="Group 2">
                        <el-menu-item index="3-3">Option 3</el-menu-item>
                    </el-menu-item-group>
                    <el-submenu index="3-4">
                        <template slot="title">Option 4</template>
                        <el-menu-item index="3-4-1">Option 4-1</el-menu-item>
                    </el-submenu>
                </el-submenu>
            </el-menu>
        </el-aside>

        <el-container>
            <el-header style="text-align: right; font-size: 12px">
                <el-dropdown>
                    <i class="el-icon-setting" style="margin-right: 15px"></i>
                    <el-dropdown-menu slot="dropdown">
                        <el-dropdown-item>View</el-dropdown-item>
                        <el-dropdown-item>Add</el-dropdown-item>
                        <el-dropdown-item>Delete</el-dropdown-item>
                    </el-dropdown-menu>
                </el-dropdown>
                <span>FPO</span>
            </el-header>

            <el-main>

                <template>
                    <el-table :data="tableData.filter(data => !search || data.fullname.toLowerCase().includes(search.toLowerCase()))" style="width: 100%">
                        <el-table-column label="Fullname" prop="fullname">
                        </el-table-column>
                        <el-table-column label="Phone" prop="phone">
                        </el-table-column>
                        <el-table-column label="Address" prop="address">
                        </el-table-column>
                        <el-table-column align="right">
                            <template slot="header" slot-scope="scope">
                                <el-input v-model="search" size="mini" placeholder="Type to search" />
                            </template>
                            <template slot-scope="scope">
                                <el-button size="mini" @click="handleEdit(scope.$index, scope.row)">Edit</el-button>
                                <el-button size="mini" type="danger" @click="handleDelete(scope.$index, scope.row)">Delete</el-button>
                            </template>
                        </el-table-column>
                    </el-table>
                </template>
                <button v-on:click="greet" type="button" class="el-button el-button--primary">
                    <span>Save</span></button>
            </el-main>
        </el-container>
    </el-container>
</div>
<script>
    var Main = {
        data() {
            return {
                tableData: JSON.parse('<?= $user ?>'),
                search: '',
                methods: {
                    handleEdit(index, row) {
                        console.log(index, row);
                    },
                    handleDelete(index, row) {
                        console.log(index, row);
                    },
                    greet: function(event) {
                        // bên trong một phương thức, `this` trỏ đến đối tượng Vue
                        alert('Xin chào ' + this.name + '!')
                        // `event` là sự kiện DOM native
                        if (event) {
                            alert(event.target.tagName)
                        }
                    }
                },
            }
        }
    }

    var Ctor = Vue.extend(Main)
    new Ctor().$mount('#app')
</script>