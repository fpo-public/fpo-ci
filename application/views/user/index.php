<div class="header">
		<a class="btn bg-deep-orange btn-raised pull-right m-t--10 waves-effect waves-light" title="Thêm thủ công" data-toggle="tooltip-t" href="<?= base_url(); ?>user/add">
			<i class="material-icons">add</i>
		</a>
		<a href="<?= base_url(); ?>admin/user/uploadExcel" class="btn bg-blue btn-raised pull-right m-t--10 waves-effect waves-light" title="Thêm bằng file Excel" data-toggle="tooltip-l">
			<i class="material-icons">cloud_upload</i>
		</a>
	<h2>DANH SÁCH TÀI KHOẢN</h2>
</div>

<div class="body">
	<div class="row clear-fix">
		<div class="col-sm-12">
			<div class="table-responsive">
				<table id="mytable" class="table table-bordered table-striped dataTable dt-responsive display nowrap" style="margin-top:10px" cellspacing="0">
					<thead>
						<tr>
							<th>#</th>
							<th>Username</th>
							<th>Fullname</th>
							<th>Phone</th>
							<th>Facebook</th>
							<th>Group</th>
							<th class="no-sort">Tác vụ</th>
						</tr>
					</thead>
					<tbody>
							<?php  if(isset($user) && is_array($user)){ $count=0; foreach($user as $u){ $count++; ?>
							<tr>
								<td><?php echo $count; ?></td>
								<td><?php echo $u['username']; ?></td>
								<td><?php echo $u['fullname']; ?></td>
								<td><?php echo $u['phone']; ?></td>
								<td><?php echo $u['facebook']; ?></td>
								<td><?php echo $u['groupName']; ?></td>
								<td>
									<a href="<?= base_url() ?>user/edit/<?= $u['_id'] ?>" class="btn btn-xs btn-info btn-raised" title="Chỉnh sửa" data-toggle="tooltip-l"><i class="material-icons">mode_edit</i></a>
									<a onclick="checkalert('<?= $u['_id'] ?>')" class="btn btn-xs btn-danger btn-raised" title="Xoá" data-toggle="tooltip-r"><i class="material-icons">delete</i></a>
								</td>
							</tr>
							<?php } } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<script>
    $.fn.dataTable.ext.errMode = 'none'; //hide warning datatable
	
	function checkalert(id) {
		swal({
				title: "Bạn đã đã chắn chắn?",
				text: "Hành động này sẽ xóa tất cả các dữ liệu liên quan, và không thể phục hồi",
				type: "warning",
				showCancelButton: true,
				cancelButtonText: "Bỏ qua",
				confirmButtonClass: "btn-danger",
				confirmButtonText: "Đồng ý!",
				closeOnConfirm: false
			},
			function() {
				swal("Thành công!", "Dữ liệu của bạn đã được xóa!", "success");
				window.location.href = "<?= base_url() ?>user/delete/" + id;
			});
	}
	
</script>






