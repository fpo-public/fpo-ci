<div class="header">
    <h2>SỬA TÀI KHOẢN</h2>
</div>

<div class="body">
    <form id="member" name="member" method="post" action="<?= base_url(); ?>User/edit/<?= $user['_id'] ?>" enctype="multipart/form-data">
        <div class="body">
            <div class="row clearfix" id="form-body">
			<div class="col-sm-12">
                    <div class="col-sm-5">
                        <div class="form-group">
                            <input type="text" name="image" value="<?= $user['avatar']; ?>" style="display:none" class="form-control" id="source_img" placeholder="Type avatar here...." />
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <a onclick="avatarClick()" title="click vào đây để thay đổi avatar"  class="btn btn-raised waves-effect waves-light" title="click vào để thay đổi avatar">
                        <?php if(isset($user['avatar']) && $user['avatar'] !="") { ?> 
                        <img src="<?= base_url().$user['avatar']; ?>" alt="" width="50px" id="prev_img" >
                        <?php } else{ ?> 
                            <img src="<?= base_url()."public/assets/images/123.png" ?>" alt="" width="50px" id="prev_img" >
                        <?php } ?></a>
                        <div class="form-line">
                            <br>
                            <label style="font-style: italic;" for="fullname">Chọn Avatar</label>
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <div class="form-group">
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 hidden" >
                    <div class="form-group">
                        <label class="control-label">Avatar</label>
                        <input type="file"  class="form-control " value="<?= $user['avatar'] ?>" id ="avarta" name="avarta">
                    </div>
                </div>
                    
                <div class="col-sm-6">
                    <label for="price">Username <strong> (*) </strong></label>
                    <div class="form-group">
                        <div class="form-line">
                            <input type="text" class="form-control" id="username" name="username" value="<?php if(isset($user['username'])) echo $user['username']; ?>" placeholder="38103001" required="" />
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <label for="rewardPoint">Password <strong> (*) </strong></label>
                    <div class="form-group">
                        <div class="form-line">
                            <input type="password" class="form-control" id="password" name="password" placeholder="password$"  />
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <label for="fullname">Họ tên <strong> (*) </strong></label>
                    <div class="form-group">
                        <div class="form-line">
                            <input type="text" id="fullname" name="fullname" class="form-control" value="<?php if(isset($user['fullname'])) echo $user['fullname']; ?>" placeholder="Nguyễn Thị Ân" required="" />
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <label for="email">Email</label>
                    <div class="form-group">
                        <div class="form-line">
                            <input type="text" id="email" name="email" class="form-control email" value="<?php if(isset($user['email'])) echo $user['email']; ?>" placeholder="ntan@gmail.com" />
                        </div>
                    </div>
                </div>
				<div class="col-sm-12">
                    <label for="Address">Địa chỉ <strong> (*) </strong> </label>
                    <div class="form-group">
                        <div class="form-line">
                            <input type="text" class="form-control" id="address" name="address" value="<?php if(isset($user['address'])) echo $user['address']; ?>" placeholder="Ex: Quận 10, Tp. HCM" required="" />
                        </div>
                    </div>
                </div> 
                <div class="col-sm-4">
                    <label for="phone">Điện thoại</label>
                    <div class="form-group">
                        <div class="form-line">
                            <input type="text" class="form-control" id="phone" name="phone" value="<?php if(isset($user['phone'])) echo $user['phone']; ?>" placeholder="Ex: 0909090909" />
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <label for="Facebook">Facebook </label>
                    <div class="form-group">
                        <div class="form-line">
                            <input type="text" class="form-control" id="facebook" name="facebook" value="<?php if(isset($user['facebook'])) echo $user['facebook']; ?>" placeholder="facebook" />
                        </div>
                    </div>
                </div>     
                <div class="col-sm-4">
                    <label for="work">Công việc</label>
                    <div class="form-group">
                        <div class="form-line">
                            <input type="text" class="form-control" id="work" name="work" value="<?php if(isset($user['work'])) echo $user['work']; ?>" placeholder="THPT ABC"  />
                        </div>
                    </div>
                </div>      
                <button type="submit" class="btn btn-block btn-lg bg-red waves-effect">LƯU</button>
            </div>
        </div>
    </form>
</div>
<script>
   

    
	$('#avarta').change(function(e){
		e.preventDefault(); 
		//Lấy ra files
		var file_data = $('#avarta').prop('files')[0];
		var userId = '<?= $user['_id'] ?>';
		//khởi tạo đối tượng form data
		var form_data = new FormData();
		form_data.append('file', file_data);
		form_data.append('userId', userId);
		//ajax post
		$.ajax({
			url: "<?= base_url()?>"+"user/updateAvarta/",
			dataType: 'text',
			cache: false,
			contentType: false,
			processData: false,
			data: form_data,
			type: 'post',
			success: function (data) {
				var info = JSON.parse(data);

				if(info.exitcode == "1"){

					$('#prev_img').attr('src',info.data);
					$('#source_img').val(info.data);
					console.log(info.data);


				}
				else{
					console.log(data);
					shownoti("glyphicon glyphicon-remove",info.data,"","","danger");
				}
			}
		});
		return false;
	});
    function previewImage(){
        var previewBox = document.getElementById("prev_img");
        previewBox.src = URL.createObjectURL(event.target.files[0]);
	}
	function avatarClick(){
        $('#avarta').click();
    }
</script>
