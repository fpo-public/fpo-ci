<!-- <script src="<?= base_url() ?>dist/jquery.fancybox.min.js"></script>  -->
<div class="header">
    <h2>THÊM TÀI KHOẢN</h2>
</div>

<div class="body">
    <form id="member" name="member" method="post" action="<?= base_url(); ?>User/add" enctype="multipart/form-data">
        <div class="body">
            <div class="row clearfix" id="form-body">
                <div class="col-sm-12">
                    <div class="col-sm-5">
                        <div class="form-group">
                                <input type="file" onchange="previewImage()" name="file" style="display:none" class="form-control" id="source_img" placeholder="Type avatar here...."  />
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <a   href="javascript:$('#source_img').click();" title="click vào đây để thay đổi avatar" data-toggle="tooltip-t" data-fancybox data-type="iframe" class = "btn btn-raised waves-effect waves-light  fancy" title="click vào để thay đổi avatar"  ><img src="<?= base_url()."public/assets/images/123.png" ?>" alt="" width="50px" id="prev_img" ></a>
                        <div class="form-line">
                            <br>
                            <label style="font-style: italic;" for="fullname">Chọn Avatar</label>
                            
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <div class="form-group">
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <label for="price">Username <strong> (*) </strong></label>
                    <div class="form-group">
                        <div class="form-line">
                            <input type="text" class="form-control" id="username" name="username" placeholder="38103001" required="" />
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <label for="rewardPoint">Password <strong> (*) </strong></label>
                    <div class="form-group">
                        <div class="form-line">
                            <input type="password" class="form-control" id="password" name="password" placeholder="password$" required="" />
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <label for="fullname">Họ tên <strong> (*) </strong></label>
                    <div class="form-group">
                        <div class="form-line">
                            <input type="text" id="fullname" name="fullname" class="form-control" placeholder="Nguyễn Thị Ân" required="" />
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <label for="email">Email</label>
                    <div class="form-group">
                        <div class="form-line">
                            <input type="text" id="email" name="email" class="form-control email" placeholder="ntan@gmail.com" />
                        </div>
                    </div>
                </div>
				<div class="col-sm-12">
                    <label for="Address">Địa chỉ <strong> (*) </strong> </label>
                    <div class="form-group">
                        <div class="form-line">
                            <input type="text" class="form-control" id="address" name="address" placeholder="Ex: Quận 10, Tp. HCM" required="" />
                        </div>
                    </div>
                </div> 
                <div class="col-sm-4">
                    <label for="phone">Điện thoại</label>
                    <div class="form-group">
                        <div class="form-line">
                            <input type="text" class="form-control" id="phone" name="phone" placeholder="Ex: 0909090909" />
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <label for="Facebook">Facebook </label>
                    <div class="form-group">
                        <div class="form-line">
                            <input type="text" class="form-control" id="facebook" name="facebook" placeholder="facebook" />
                        </div>
                    </div>
                </div>     
                <div class="col-sm-4">
                    <label for="work">Công việc</label>
                    <div class="form-group">
                        <div class="form-line">
                            <input type="text" class="form-control" id="work" name="work" placeholder="THPT ABC"  />
                        </div>
                    </div>
                </div>      
                <button type="submit" class="btn btn-block btn-lg bg-red waves-effect">LƯU</button>
            </div>
        </div>
    </form>
</div>
<script>
   
//    $(document).ready(function(){
//         $('.fancy').fancybox({	
//             'width'		: 900,
//             'height'	: 600,
//             'type'		: 'iframe',
//             'autoScale'    	: true
//         });
//     });
//     function responsive_filemanager_callback(field_id){
//         var image = $('#' + field_id).val();
// 		$('#prev_img').attr('src',image);
//     }
    
    function previewImage(){
        var previewBox = document.getElementById("prev_img");
        previewBox.src = URL.createObjectURL(event.target.files[0]);
    }
</script>
