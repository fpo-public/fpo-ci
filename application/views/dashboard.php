<div class="header">
    <h2>TỔNG QUAN</h2>
</div>
<div class="body">
    <!-- Widgets -->
    <div class="row clearfix">
        <div class="col-lg-6 col-lg-6 col-sm-6 col-xs-12">
            <div class="info-box bg-pink hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">chrome_reader_mode</i>
                </div>
                <div class="content">
                    <div class="text">Luợt checkin</div>
                    <div class="number count-to" data-from="0" data-to="<?php if(isset($report->nAttendance)) echo $report->nAttendance; ?>" data-speed="1000" data-fresh-interval="20"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-lg-6 col-sm-6 col-xs-12">
            <div class="info-box bg-light-green hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">check_box</i>
                </div>
                <div class="content">
                    <div class="text">Tổng sự kiện</div>
                    <div class="number count-to" data-from="0" data-to="<?php if(isset($report->nEvent)) echo $report->nEvent; ?>" data-speed="1000" data-fresh-interval="20"></div>                            
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-lg-6 col-sm-6 col-xs-12">
            <div class="info-box bg-cyan hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">check_box</i>
                </div>
                <div class="content">
                    <div class="text">Tổng số khách mời</div>
                    <div class="number count-to" data-from="0" data-to="<?php if(isset($report->nParticipan)) echo $report->nParticipan; ?>" data-speed="1000" data-fresh-interval="20"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-lg-6 col-sm-6 col-xs-12">
            <div class="info-box bg-orange hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">person_outline</i>
                </div>
                <div class="content">
                    <div class="text">Tổng số user</div>
                    <!-- <div class="number count-to" data-from="0" data-to="<?php if(isset($report->nUser)) echo $report->nUser; ?>" data-speed="1000" data-fresh-interval="20"></div> -->
                    <div class="number count-to" data-from="0" data-to="10000" data-speed="1000" data-fresh-interval="20"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Widgets -->

    <div class="row clearfix">
        <!-- USERS -->
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
            <div class="card">
                <div class="body bg-light-blue">
                    <div class="font-bold m-b--35">ĐOÀN VIÊN THUỘC ĐOÀN CƠ SỞ</div>
                    <ul class="dashboard-stat-list">
                        <li>
                            abc
                            <span class="pull-right"><b>123</b> <small>ĐOÀN VIÊN</small></span>
                        </li>
                        
                    </ul>
                </div>
            </div>
        </div>
        <!-- #END# USERS -->
        <!-- RLĐV -->
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
            <div class="card">
                <div class="body bg-cyan">
                    <div class="font-bold m-b--35">KẾT QUẢ PHÂN TÍCH RLĐV</div>
                    <ul class="dashboard-stat-list">
                        <li>
                            abc
                            <span class="pull-right"><b>123</b> <small>ĐOÀN VIÊN</small></span>
                        </li>

                    </ul>
                </div>
            </div>
        </div>
        <!-- #END# RLĐV -->
        <!-- CATEGORY_NEWS STAT -->
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
            <div class="card">
                <div class="body bg-cyan">
                    <div class="font-bold m-b--35">KẾT QUẢ PHÂN TÍCH RLĐV</div>
                    <ul class="dashboard-stat-list">
                        <li>
                            abc
                            <span class="pull-right"><b>123</b> <small>ĐOÀN VIÊN</small></span>
                        </li>

                    </ul>
                </div>
            </div>
        </div>
        <!-- #END# CATEGORY_NEWS STAT -->
    </div>
</div>


