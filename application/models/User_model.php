<?php
/* 
 * Generated by CRUDigniter v3.2 
 * www.crudigniter.com
 */
 
class User_model extends CI_Model implements CrudModel
{
    function __construct()
    {
        parent::__construct();
    }
    
    function getAll()
    {
        $curl_post_data = array(
        );
        $res=restAPI($this->config->item('linkAPI')."/user/getAll",arraytostring($curl_post_data));
        return $res;
    }

    function getById($curl_post_data)
    {

        $res=restAPI($this->config->item('linkAPI')."/user/getById",arraytostring($curl_post_data));
        return $res;
    }
    function create($curl_post_data)
    {
        
        $res=restAPI($this->config->item('linkAPI')."/user/create",arraytostring($curl_post_data));
        return $res;
    }
    function update($curl_post_data)
    {
        // return $curl_post_data;
        $res=restAPI($this->config->item('linkAPI')."/user/update",arraytostring($curl_post_data));
        return $res;
    }
    function delete($curl_post_data)
    {
        $res=restAPI($this->config->item('linkAPI')."/user/delete",arraytostring($curl_post_data));
        return $res;
    }
    
}
