<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Qrscan extends FPO_Controller {

	function __construct ()
	{	
		parent::__construct();
		$this->load->library('phpqrcode/qrlib');
		$this->load->helper('url');
		
		$_SESSION['headerTitle'] = "Attendance system";
		$_SESSION['navi'] = 'qrcode';
	}

	public function index()
	{	
		$_SESSION['navi'] = 'qrcode-scan';
		$_SESSION['headerTitle'] = '[[EVENT_NAME]] - Attendance system | FPO Co.,Ltd';
		// $this->load->view('include-template/header.php');
		$data['_view'] = 'page/profileScan';
        $this->load->view('layouts/scan',$data);
	}
	function add(){return true;}
	function edit($id){return true;}
	function delete($id){return true;}
	public function qrcodeGenerator()
	{
		
		$_SESSION['navi'] = 'qrcode-genegate';
		
		// $qrtext = $this->input->post('qrcode_text');
		$qrtext = "https://play.google.com/store/apps/details?id=com.garastem&fbclid=IwAR3-wOTKweAnxNB5GY_6QNv-pDqkXeGrNW5nEyfTpvidhzm4JzaV1c3emDU";
		if(isset($qrtext))
		{
			//file path for store images
		    $SERVERFILEPATH = $_SERVER['DOCUMENT_ROOT'].'/attendance-sys-web/images/qrcode/';
		   
			$text = $qrtext;
			$folder = $SERVERFILEPATH;
			$url = $text.".png";
			// $url = "qrcode".".png";
			$file_name = $folder.$url;
			QRcode::png($text,$file_name);
			
			echo"<center><img src=".base_url().'images/qrcode/'.$url."></center";
		}
		else
		{
			echo 'No Text Entered';
		}	
	}
}
