<?php
class Auth extends Auth_Controller 
{
    public function __construct()
    {
        parent::__construct();
		$_SESSION['headerTitle'] = "Attendance system";

	}
    public function index()
    {
       
		$this->load->view('auth/login');
    }
    public function forget(){
        $noti = array(
            'icon' => 'glyphicon glyphicon-share-alt',
            'title' => "",
            'message' => "Vui lòng kiểm tra email để lấy lại mật khẩu!",
            'url' => "javascript:void(0)",
            'type' => 'info'
        );
        $_SESSION["THONGBAO"]=$noti;
        $this->load->view('auth/forgetPassword');
    }
    public function test()
    {
        $noti = array(
            'icon' => 'glyphicon glyphicon-share-alt',
            'title' => "",
            'message' => "Vui lòng kiểm tra email để lấy lại mật khẩu!",
            'url' => "javascript:void(0)",
            'type' => 'info'
        );
        $_SESSION["THONGBAO"]=$noti;
        $data['_view'] = 'user/add';
        $this->load->view('layouts/main',$data);
    }
    // public function login(){
    //     $username=$this->input->post('username');
    //     $password=$this->input->post('password');
    //     $curl_post_data = array(
    //         "username" =>$username,
    //         "password" =>md5($password)
    //     );
    //     $kq = restLogin($this->config->item('linkAPI')."/oauth",arraytostring($curl_post_data));
    //     // print_r($kq);return;
    //     if(isset($kq->exitcode)){
    //         if($kq->exitcode==101){
    //             echo '{"exitcode":"101" ,"mess":"Username is existed"}';                
    //             return;
    //         }
    //         else if($kq->exitcode==102){
    //             echo '{"exitcode":"102" ,"mess":"Valid username"}';                
    //             return;
    //         }
    //             else if($kq->exitcode==103){
    //             echo '{"exitcode":"103" ,"mess":"Thiếu thông tin truy cập"}';                
    //             return;
    //         }
    //             else if($kq->exitcode==104){
    //             echo '{"exitcode":"104" ,"mess":"Sai tài khoản hoặc mật khẩu"}';                
    //             return;
    //         }
    //         else if($kq->exitcode==0){
    //             echo '{"exitcode":"0" ,"mess":"exception error!"}';
    //             return;
    //         }
    //         else if($kq->exitcode==2){
    //             echo '{"exitcode":"2" ,"mess":"No data!"}';
    //             return;
    //         }
    //     }
    //     if(isset($kq->data->token)){
    //         $_SESSION['token'] =$kq->data->token;
            
    //         $curl_post_data = array(
    //             "token" =>$kq->data->token
    //         );
    //         $userDecode = json_decode(restAPItest($this->config->item('linkAPI')."/system/decode",arraytostring($curl_post_data)));
    //         if($userDecode != ""){
    //             $_SESSION['attendanceUser']=$userDecode;
    //             echo '{"exitcode":"1" ,"mess":"success"}';
    //             return;

    //         }
           
    //     }
    //     else{
    //         echo '{"exitcode":"0" ,"mess":"exception error!"}';
                
    //         return;
    //     }
    //     return;
    // }
    public function login(){
        $username=$this->input->post('username');
        $password=$this->input->post('password');
        $curl_post_data = array(
            "username" =>$username,
            "password" =>md5($password)
        );
        $kq = (restAPINoToken($this->config->item('linkAPI')."/oauth",arraytostring($curl_post_data)));
        if(isset($kq['exitcode']) && $kq['exitcode'] == "905"){
            echo json_encode($kq);
            return;
        }
        $_SESSION['token'] =$kq['token'];
        $curl_post_data = array();
        $userDecode = (restLogin($this->config->item('linkAPI')."/system/decode",arraytostring($curl_post_data)));
        // print_r($userDecode);return;
        if(isset($userDecode['exitcode']) && $userDecode['exitcode'] == "1"){
            
            // print_r($userDecode);
            $_SESSION['fpoUser']=$userDecode['data'];
            
            // $listPermission = (restAPI($this->config->item('linkAPI')."/user/getPermission",arraytostring($curl_post_data)));
            // $_SESSION['fpoPermission'] = $listPermission;
            echo '{"exitcode":"1" ,"mess":"success"}';
            return;

        }
        else if(isset($userDecode['exitcode']) && $userDecode['exitcode'] == "0"){
            echo '{"exitcode":"0" ,"mess":"Sai tài khoản hoặc mật khẩu!"}';
            return;
        }
        else{
            echo '{"exitcode":'.$userDecode['exitcode'].' ,"mess":'.$userDecode['message'].'}';
            return;

        }
        
        return;
    }
    public function recover()
    {
        $noti = array(
            'icon' => 'glyphicon glyphicon-share-alt',
            'title' => "",
            'message' => "Liên hệ với admin để lấy lại mật khẩu!",
            'url' => "javascript:void(0)",
            'type' => 'info'
        );
        $_SESSION["THONGBAO"]=$noti;
        redirect('/');
    }
    
    public function resetPassword(){
        $this->load->model('User_m');

        $newPassword = $this->input->post('new-password');
        $confirmPassword = $this->input->post('confirm-password');
        $resetToken = $this->input->post('resetToken');
        $resetCode = $this->input->post('resetCode');
		$res= $this->User_m->resetPassword($newPassword,$confirmPassword,$resetToken,$resetCode);
         
        // $rs = json_decode($res);
        print_r($res);
        return;
	}
    public function logout()
    {
        session_destroy();
        redirect('/Auth');
    }
}
