<?php 

abstract  class FPO_Controller extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        session_start();
        if($this->config->item('linkAPI') ==''){redirect('Install');}
        if(!isset($_SESSION['fpoUser']))
        {
            $noti = array(
				'icon' => 'glyphicon glyphicon-star',
				'title' => "",
				'message' => "Đăng nhập để sử dụng hệ thống!",
				'url' => "javascript:void(0)",
				'type' => 'warning'
			);
			$_SESSION["THONGBAO"]=$noti;
            return redirect("/Auth");
        }
        $this->interface("interfaceModel");
        //  $this->CheckPermission();
    }
    abstract protected function index();
    abstract protected function add();
    abstract protected function edit($id);
    abstract protected function delete($id);
    
    private function interface($strInterfaceName) {
        require_once APPPATH . '/interfaces/' . $strInterfaceName . '.php';
    }
    public function CheckPermission(){
        $this->load->model('User_model');
        $user = $this->User_model->get_username($_SESSION['user']->username);
        $_SESSION['listPermission'] = (array)json_decode(strtolower($user['function']));

        $class = strtolower($this->router->fetch_class());
        $method = strtolower($this->router->fetch_method());
        if(!(isset($_SESSION['listPermission'][$class]) && in_array($method,$_SESSION['listPermission'][$class]))){
            show_error("Bạn không có quyền truy cập trang này" , 403 );
        }
    }
    
    
}
abstract class Auth_Controller extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library("user_agent");
        session_start();
        if($this->config->item('linkAPI') ==''){redirect('Install');}
        $this->interface("interfaceModel");
    }
    abstract protected function login();
    abstract protected function logout();
    abstract protected function forget();
    abstract protected function recover();
    abstract protected function index();
    
    private function interface($strInterfaceName) {
        require_once APPPATH . '/interfaces/' . $strInterfaceName . '.php';
    }
    
    
    
}
