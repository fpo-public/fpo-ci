$(function () {
    try {
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="tooltip-r"]').tooltip({ placement: 'right' });
        $('[data-toggle="tooltip-l"]').tooltip({ placement: 'left' });
        $('[data-toggle="tooltip-t"]').tooltip({ placement: 'top' });
        $('[data-toggle="tooltip-b"]').tooltip({ placement: 'bottom' });
    } catch (err) { console.log(err.message) }

    try {
        $('.form-control.datetime').inputmask('d/m/y h:s', { placeholder: '__/__/____ __:__', alias: "datetime", hourFormat: '24' });
        $('.form-control.date').inputmask('dd/mm/yyyy', { placeholder: '__/__/____' });
        $('.form-control.mssv').inputmask('99.999.999', { placeholder: '__.___.___' });
        $('.form-control.email').inputmask({ alias: "email" });
        $('.form-control.phone').inputmask('9999 9999 [999]', { placeholder: '___ ____ ____' });
        $('.form-control.number').inputmask({ alias: "integer", rightAlign: false });
    } catch (err) { console.log(err.message) }
    try { activateNotificationAndTasksScroll(); } catch (err) { console.log(err.message) }
    if ($('.dataTable.ajax').length >= 1) {
        try {
            $('.dataTable').DataTable({
                responsive: true,
                stateSave: true,
                processing: true,
                "language":
                {
                    "decimal": "",
                    "emptyTable": "Dá»¯ liá»‡u rá»—ng",
                    "info": "Hiá»ƒn thá»‹ tá»« _START_ Ä‘áº¿n _END_ trong tá»•ng cá»™ng _TOTAL_ dĂ²ng",
                    "infoEmpty": "Dá»¯ liá»‡u rá»—ng",
                    "infoFiltered": "(tĂ¬m kiáº¿m tá»« _MAX_ dĂ²ng)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": '',
                    "loadingRecords": "Loading...",
                    "processing": "Processing...",
                    "search": '<label class="control-label">TĂ¬m kiáº¿m</label>',
                    "zeroRecords": "KhĂ´ng tĂ¬m tháº¥y",
                    "paginate": {
                        "first": "Äáº§u",
                        "last": "Cuá»‘i",
                        "next": "Â»",
                        "previous": "Â«"
                    },
                    "aria": {
                        "sortAscending": ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                    }
                },
                "aoColumnDefs": [{
                    "bSortable": false,
                    "aTargets": ["no-sort"]
                }]
            });
        } catch (err) { console.log(err.message) }
    }

    $(window).resize(function () {
        try { setSkinListHeightAndScroll(); } catch (err) { console.log(err.message) }
        try { setSettingListHeightAndScroll(); } catch (err) { console.log(err.message) }
    });
});

var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';

//Activate notification and task dropdown on top right menu
function activateNotificationAndTasksScroll() {
    $('.navbar-right .dropdown-menu .body .menu').slimscroll({
        height: '254px',
        color: 'rgba(0,0,0,0.5)',
        size: '4px',
        alwaysVisible: false,
        borderRadius: '0',
        railBorderRadius: '0'
    });
}